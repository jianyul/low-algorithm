# 准确时间获取

本项目基于https://blog.csdn.net/xy3233/article/details/122405558 实现；使用Python实现编程；
授时地址参考https://blog.csdn.net/james1989vip/article/details/113830683 ；
简单获取时间得两种方法，一使用网站，二使用NTP授时。

# Todo

当前，只实现了从网络上获取时间，为进行后期处理。

后期希望添加程序处理，实现时钟界面。