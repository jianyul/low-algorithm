# -*- coding: utf-8 -*-
import datetime
import time
import ntplib
import requests
"""
本项目基于https://blog.csdn.net/xy3233/article/details/122405558实现；
授时地址参考https://blog.csdn.net/james1989vip/article/details/113830683；
简单获取时间得两种方法，一使用网站，二使用NTP授时。
"""
#使用网站获取北京时间
def get_beijin_time():
    try:
        url = 'https://beijing-time.org/'
        request_result = requests.get(url=url)
        if request_result.status_code == 200:
            headers = request_result.headers
            net_date = headers.get("date")
            gmt_time = time.strptime(net_date[5:25], "%d %b %Y %H:%M:%S")
            bj_timestamp = int(time.mktime(gmt_time) + 8 * 60 * 60)
            return datetime.datetime.fromtimestamp(bj_timestamp)
    except Exception as exc:
        return datetime.datetime.now()

# 获取当前NTP时间
def get_ntp_time():
    ntp_client = ntplib.NTPClient()
    response = ntp_client.request('time.edu.cn')

    #国家授时中心ntp服务器地址
    #ntp.ntsc.ac.cn
    #教育网内授时服务器地址
    #time.edu.cn
    #阿里NTP
    #ntp.aliyun.com
    #腾讯NTP
    #ntp.tencent.com
    return datetime.datetime.fromtimestamp(response.tx_time)

if __name__ == '__main__':
    #网站北京时间
    bj_time = get_beijin_time()
    print("北京时间: ", bj_time)
    #NTP时间
    ntp_time = get_ntp_time()
    print("ntp时间: ", ntp_time)