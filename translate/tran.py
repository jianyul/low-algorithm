#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import subprocess

##打开配置文件
with open("config.json","r",encoding="utf-8") as f:
    load_dict = json.load(f)##将JSON数据转为字典

##测试打印结果
# print(type(load_dict))
# print(load_dict)
# print(load_dict["dir"])
# print(load_dict["duak"][0])

# command = "cd " + load_dict["dir"] + " && " + load_dict["duak"][0]
# print(command)


#command = ['dir',load_dict["dir"]]
# command = 'dir'+' '+load_dict["dir"]
# print(command)

command = [load_dict["dir"]+'\\'+'ffmpeg','-i',load_dict["dir"]+'\\'+load_dict["input"],load_dict["dir"]+'\\'+load_dict["output"]]

##注意这里的shell=True，与python3的版本有关，该参数是python3.12才有的
subprocess.run(command,shell=True,check=True)

#subprocess.Popen(command,shell=True)