# 使用python调用ffmpeg进行视频转码
使用json文件进行参数和文件路径的配置

使用python调用ffmpeg进行视频转码


## 参考设计
如何调用ffmpeg

https://blog.csdn.net/Fandes_F/article/details/105148407

https://docs.pingcode.com/baike/861216

如何调用CMD

https://blog.csdn.net/Rocky006/article/details/131154694

读取配置json（config）

https://www.cnblogs.com/CircleWang/p/15270974.html

官方参考

https://ffmpeg.org/documentation.html

https://docs.python.org/zh-cn/3/library/subprocess.html#module-subprocess


## 总结

通过该代码学习如下内容：

1.如何编写json文件

2.如何使用python读取json文件

3.如何使用命令工具调用本地ffmpeg，这里使用subprocess命令行库run方法实现。

## 存在问题和todo
1.只是简单实现代码，并且与windos11的python3.12高度依赖，后续需要添加代码的兼容性。

2.ffmpeg的调用需要添加参数，为后面的转码提供更多的参数。





